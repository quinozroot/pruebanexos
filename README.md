Proyecto Nexos

Tecnologia Implementada:

* Spring Boot
* Hibernate
* JPA
* MYSQL
* AOP (Programacion Orientada a Aspectos)
* Apache Tomcat

Descripcion.

Creacion de un microservicio.

Observacion:

En la carpeta llamada "Prueba", se encuentra el codigo fuente generado para el proyecto.
En la Carpeta llamada "SQL", se encuentra un Script con el cual se pobla la tabla nexos.tipo_identificacion.


Como Instalar:

1. Cree la base de datos(Si no utilice la que esta por defecto y omita los pasos de bases de datos).
2. Descarge el proyecto.
3. Ejecutelo (El crea toda la estructura de la base de datos si lo esta realizando en una base de datos nueva).
4. Si esta creando la base de datos desde 0 ejecute el Script que se encuentra en la carpeta SQL, Si no omita.
5. Pruebe la aplicacion desde Postman mas recomendado.

Cabe mencionar que todos los puntos indicados en el documento fueron resueltos, el control de log quedo como POA (Programacion orientada a aspectos)

Quedo atento.