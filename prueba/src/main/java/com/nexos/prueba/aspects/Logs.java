/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.aspects;

import com.google.common.io.CharStreams;
import com.nexos.prueba.controller.SystemExceptionController;

import com.nexos.prueba.entity.SystemException;
import com.nexos.prueba.exception.NexosException;
import com.nexos.prueba.util.ConvertJsonUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.data.repository.init.ResourceReader.Type.JSON;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author USUARIO Clase POA, Esta clase permite el capturar los datos en tiempo
 * de ejecucion utilizando puntos de corte con el fin de generar un trazailidad
 * de auditorio para controlar todo el ciclo de vida de la peticion a la cual se
 * le de la orden de monitoriar
 */
@Aspect
@Component
public class Logs {

  
    @Autowired
    private SystemExceptionController systemExceptionController;

    @Autowired
    private ConvertJsonUtil convertJsonUtil;

    /**
     * cuando se ejecuta el punto de corte se va a ejecutar este metodo que
     * funciona Antes de ejecutar el webmethod
     *
     * @param pjp Se obtiene todos los datos que vienen del punto de corte
     * @return Object objecto generado por la peticion, este objecto es dinamico
     * y se acomoda a cada peticion
     * @throws Throwable Error sin control en el sistema
     */
   /* @Around("execution(* com.nexos.prueba.api.*.*(..))")
    public Object processTime(ProceedingJoinPoint pjp) throws Throwable {

        //Se obtiene la peticion
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        try {
            //Variable de auditoria
            LogRequest logRequest = new LogRequest();

            logRequest.setMethod(request.getMethod());
            logRequest.setUrl(request.getRequestURL().toString());

            String _requestBody = this.getRequest(request, pjp);

            logRequest.setRemoteAddr(request.getRemoteHost());
            logRequest.setRequestBody(_requestBody);
            logRequest.setTimeIni(Date.from(Instant.now()));

            //Obtiene la fecha antes de ejecutar el metodo
            Calendar before = Calendar.getInstance();
            System.out.println(before);
            //Ejecuta el llamado al webmethod al cual fue invocado
            Object obj = pjp.proceed();

            //Obtiene la fecha luego de ejecutar el metodo
            Calendar now = Calendar.getInstance();
            System.out.println(now);
            //Obtiene el response que genero el microservicio
            HttpServletResponse _response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();

            logRequest.setTimeEnd(Date.from(Instant.now()));
            logRequest.setResponseBody(this.getResponse(request, pjp));
            //Giardamos el error controlado en la base de datos
            this.logRequestController.save(logRequest);

            return obj;
        } catch (NexosException e) {

            //Obtenemos todos los datos almacenar
            SystemException systemException = new SystemException();
            //Casteamos el objecto a guardar
            systemException.setError(e.getMensaje());
            systemException.setRequestBody(e.getJson());
            systemException.setUrl(request.getRequestURL().toString());

            this.systemExceptionController.save(systemException);

            return new ResponseEntity<>(e.getMensaje(), e.getHttpStatus());
        } catch (Exception ex) {
            //Error sin control en el software  
            //Se almacena el error en la base de datos sobre el error del servidor
            //Obtenemos todos los datos almacenar
            SystemException systemException = new SystemException();
            //Casteamos el objecto a guardar
            systemException.setError(ex.getMessage());

            String _requestBody = "";

            if (request.getAttribute("requestBody") != null) {
                _requestBody = (String) request.getAttribute("requestBody");
            }
            systemException.setRequestBody(_requestBody);
            systemException.setUrl(request.getRequestURL().toString());

            this.systemExceptionController.save(systemException);
            //Error de retorno
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }*/
    /**
     * 
     * @param request Peticion a la solicitud
     * @param pjp Punto de corte creado
     * @return String El curerpo de la peticion
     * @throws Exception Error controlado
     */
    public String getRequest(HttpServletRequest request, ProceedingJoinPoint pjp) throws Exception {
        if ("POST".equals(request.getMethod()) || "PUT".equals(request.getMethod())) {

            if (pjp.getArgs().length > 0) {
                for (Object o : pjp.getArgs()) {
                    if (o instanceof HttpServletRequest || o instanceof HttpServletResponse) {
                        continue;
                    }
                    return this.convertJsonUtil.toJson(o);
                }
            }
        }
        return "";
    }

    /**
     * 
     * @param request Respuesta de la solicitud
     * @param pjp punto de corte creeado
     * @return String cuerpo de la respuesta
     * @throws Exception  Error controlado
     */
    public String getResponse(HttpServletRequest request, ProceedingJoinPoint pjp) throws Exception {

        if (!"POST".equals(request.getMethod()) && !"PUT".equals(request.getMethod())) {
            Object o = pjp.getTarget();
                if (o instanceof HttpServletRequest || o instanceof HttpServletResponse) {
                }
                return this.convertJsonUtil.toJson(o);
            

        } else {
            for (Object o : pjp.getArgs()) {
                if (o instanceof HttpServletRequest || o instanceof HttpServletResponse) {
                    continue;
                }
                return this.convertJsonUtil.toJson(o);
            }
        }

        return "";
    }

}
