/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.exception;

import org.springframework.http.HttpStatus;

/**
 *
 * @author USUARIO
 * Clase que personaliza la exception creada
 */
public class NexosException extends Exception{
	
    private String mensaje;
    private HttpStatus httpStatus;
    private String json;

    
    /**
     * 
     * @param mensaje Mensaje a ser enviado a mostrar para el usuario
     * @param httpStatus Codigo Http para el error segun la falla
     * @param json Objecto que tiene la peticion para ser agregada al log
     */
    public NexosException(String mensaje, HttpStatus httpStatus, String json) {
        this.mensaje = mensaje;
        this.httpStatus = httpStatus;
        this.json = json;
    }

    
    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the httpStatus
     */
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    /**
     * @param httpStatus the httpStatus to set
     */
    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    /**
     * @return the json
     */
    public String getJson() {
        return json;
    }

    /**
     * @param json the json to set
     */
    public void setJson(String json) {
        this.json = json;
    }
    
    
    
    

    
        
        
        
        
		
}
