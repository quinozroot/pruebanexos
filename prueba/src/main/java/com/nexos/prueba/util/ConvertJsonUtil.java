/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.IOException;
import java.io.StringWriter;
import org.springframework.stereotype.Component;

/**
 *
 * @author USUARIO
 *
 * Permite convertir los objectos a JSON
 */
@Component
public class ConvertJsonUtil {

    
    /**
     * 
     * @param obj Es el objecto que se quiere convertir a JSON
     * @return String En la variable va el Json que se quiere convertir
     * @throws Exception Error Generado
     */
    public String toJson(Object obj) throws Exception {

        try {
            // Se crea un Objecto mapiador
            ObjectMapper objectMapper = new ObjectMapper();
            //Configura el objecto para ser pintado
            objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);

            //Escribe el objecto en consola 
            StringWriter stringEmp = new StringWriter();
            objectMapper.writeValue(stringEmp, obj);
            
            //Retorna el Json creado
            return "" + stringEmp;

        } catch (IOException e) {
            //Genera un error de retorno
            throw e;
        }

    }

}
