/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.repository;

import com.nexos.prueba.entity.SystemException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author USUARIO
 * Esta interface permite acceder a los datos por persistencia
 */
@Repository
public interface SystemExceptionRepository extends JpaRepository<SystemException, Integer>{
    
}
