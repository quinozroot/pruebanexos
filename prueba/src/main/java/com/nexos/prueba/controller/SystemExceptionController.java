/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.controller;

import com.nexos.prueba.entity.SystemException;
import com.nexos.prueba.service.SystemExceptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author USUARIO
 * 
 * Errores del sistema
 */
@Controller
public class SystemExceptionController {
    
    //Instancia del servicio a utilizar
    @Autowired
    private SystemExceptionService systemExceptionService;
    
    
    public void save(SystemException systemException){
        this.systemExceptionService.save(systemException);
    }
    
}
