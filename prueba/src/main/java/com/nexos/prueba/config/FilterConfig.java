/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.config;

import com.nexos.prueba.filter.AuthorizationFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author USUARIO
 */
@Configuration
public class FilterConfig {
	 //Metodo que me permite acceder a un filter para controlar las peticiones
	 @Bean
	 public AuthorizationFilter authorizationFilter() {
		 AuthorizationFilter filter = new AuthorizationFilter();
		 return filter;
	 } 

}
