/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 *
 * @author USUARIO
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = ResourceNames.ASPECT)
public class AspectConfig {
    
}
