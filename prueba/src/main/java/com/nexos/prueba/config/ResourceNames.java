/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.config;

/**
 *
 * @author USUARIO
 */

/**Clase que me almacena los nombres de los recursos a utilizar en el sistema
 * Con el fin de tener control de los paquetes utilizados
*/
    
public class ResourceNames {
    
    
    public static final String PROPERTIES = "classpath:META-INF/application.properties";

    public static final String ASPECT = "com.nexos.prueba.aspects";

    public static final String REST_API = "com.nexos.prueba.api";

    public static final String CONTROLLERS = "com.nexos.prueba.controller";

    public static final String SERVICES = "com.nexos.prueba.service";
    
    public static final String FILTER = "com.nexos.prueba.filter";
    
}
