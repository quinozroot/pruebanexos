/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.wrapper;

/**
 *
 * @author USUARIO
 * 
 * Clase que sirve para enviar y recibir la informacion del usuario
 * al momento de crear o actualizar
 * 
 */
public class UserWrapper {
    
   
    private Integer tipoIdentificacion;
    private String nombre;
    private String apellido;
    private Integer edad;
    private String numDocumento;
    private Integer idNucleo;
    private Boolean nucleoActivo;

    
    
    /**
     * Constructor de la clase
     * @param tipoIdentificacion identificacion del usuario
     * @param nombre nombre del usuario 
     * @param apellido apellido del usuario
     * @param edad edad del usuario
     * @param numDocumento numero de documento del usuario
     * @param idNucleo id del nucleo si se va a incorporar a uno existente, si no dejarlo en 0
     * @param nucleoActivo  dato que permite saber si va a crear un grupo nuevo o va a ingresar a unoexistente
     */
    public UserWrapper(Integer tipoIdentificacion, String nombre, String apellido, Integer edad, String numDocumento, Integer idNucleo, Boolean nucleoActivo) {
        this.tipoIdentificacion = tipoIdentificacion;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.numDocumento = numDocumento;
        this.idNucleo = idNucleo;
        this.nucleoActivo = nucleoActivo;
    }

    public UserWrapper() {
    }
    
    /**
     * @return the tipoIdentificacion
     */
    public Integer getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * @param tipoIdentificacion the tipoIdentificacion to set
     */
    public void setTipoIdentificacion(Integer tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @return the edad
     */
    public Integer getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    /**
     * @return the numDocumento
     */
    public String getNumDocumento() {
        return numDocumento;
    }

    /**
     * @param numDocumento the numDocumento to set
     */
    public void setNumDocumento(String numDocumento) {
        this.numDocumento = numDocumento;
    }

    /**
     * @return the idNucleo
     */
    public Integer getIdNucleo() {
        return idNucleo;
    }

    /**
     * @param idNucleo the idNucleo to set
     */
    public void setIdNucleo(Integer idNucleo) {
        this.idNucleo = idNucleo;
    }

    /**
     * @return the nucleoActivo
     */
    public Boolean getNucleoActivo() {
        return nucleoActivo;
    }

    /**
     * @param nucleoActivo the nucleoActivo to set
     */
    public void setNucleoActivo(Boolean nucleoActivo) {
        this.nucleoActivo = nucleoActivo;
    }
    
    /**
     * Metodo que permite saber si los datos vienen completos
     * @return Boolean dato que identifica si estan correctos o no los datos
     */
    public Boolean validateData(){
       
        Boolean flag = false;
        if(getTipoIdentificacion() != null){
            if(!nombre.isEmpty()){
                if(!apellido.isEmpty()){
                    if(getEdad() != null){
                        if(!numDocumento.isEmpty()){
                            flag = true;
                        }
                    }
                }
            }
        }
        return flag;
    }


}

