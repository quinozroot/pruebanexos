/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.service;

import com.nexos.prueba.entity.SystemException;
import com.nexos.prueba.repository.SystemExceptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author USUARIO
 * Esta clase permite generar una asociacion entre el Controlador y el acceso a datos
 */
@Service
@Transactional
public class SystemExceptionService {
    
    //Instancia del repositorio
    @Autowired
    private SystemExceptionRepository systemExceptionRepository;
    
    
    public void save(SystemException systemException){
        this.systemExceptionRepository.save(systemException);
    }
    
    
}
