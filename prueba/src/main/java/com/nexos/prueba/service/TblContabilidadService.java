/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.service;

import com.nexos.prueba.entity.TblContabilidad;
import com.nexos.prueba.repository.TblContabilidadRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author USUARIO
 */
@Service
@Transactional
public class TblContabilidadService {
    
    @Autowired
    private TblContabilidadRepository tblContabilidadRepository;
    
    
    public TblContabilidad save (TblContabilidad tblContabilidad){
        return this.tblContabilidadRepository.save(tblContabilidad);
    }
    
    public TblContabilidad findOne(Integer id){
        return this.tblContabilidadRepository.findOne(id);
    }
    
    public List<TblContabilidad> findAll(){
        return this.tblContabilidadRepository.findAll();
    }
    
}
