/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.api;

import com.nexos.prueba.entity.TblContabilidad;
import com.nexos.prueba.service.TblContabilidadService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author USUARIO
 */
@RestController
@RequestMapping("/v1/contabilidad")
public class TblContabilidadApi {
    
    
    @Autowired
    private TblContabilidadService tblContabilidadService;
    
    @PostMapping(value="")
    public ResponseEntity<TblContabilidad> save(@RequestBody TblContabilidad tblContabilidad) throws Exception {
        
        //Se obtienen todos los documentos que existen en el sistema
        TblContabilidad contabilidad = this.tblContabilidadService.save(tblContabilidad);
        
        //Mensaje de error correcto
        return new ResponseEntity(contabilidad,HttpStatus.OK);
    }
    
    @GetMapping(value = "/all")
    public ResponseEntity<List<TblContabilidad>> getAllId() throws Exception {
        
        //Se obtienen todos los documentos que existen en el sistema
        List<TblContabilidad> contabilidades = this.tblContabilidadService.findAll();
        
        //Mensaje de error correcto
        return new ResponseEntity(contabilidades,HttpStatus.OK);
    }
    
}
