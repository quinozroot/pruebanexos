/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author USUARIO
 */

/*
Clase de persistencia a la base de datos que me permite hacer uso
Y tener control sobre la entidad system_exception en la base de datos generando una
persistencia con el fin de poder ejecutar metodos de CRUD sobre la entidad y sus relacion
*/

//Declaramos la entidad
@Entity
//Declaramos el nombre de la tabla a manejar
@Table(name="system_exception")
public class SystemException implements Serializable {

    //declaro el constructor para que me inicialize esas variables con el fin
    //de tenerlos cargados al momento de generar una nueva instancia
    public SystemException() {
        this.createdAt = Date.from(Instant.now());
        this.updatedAt = Date.from(Instant.now());
    }
    
    //con las etiquetas declaramos que es una llave primaria y es auto_increment
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)    
    private Integer id;
    
    //Se da el valor al nombre de la columna
    @Column(name="error")
    private String error;
    
    //Se da el valor al nombre de la columna
    @Column(name="url")
    private String url;
    
    //Se da el valor al nombre de la columna
    @Column(name="request_body", length = 1000)
    private String requestBody;
    
    //Se da el valor al nombre de la columna
    @Column(name="created_at")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date createdAt;
    
    //Se da el valor al nombre de la columna
    @Column(name="updated_at")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date updatedAt;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the requestBody
     */
    public String getRequestBody() {
        return requestBody;
    }

    /**
     * @param requestBody the requestBody to set
     */
    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    
    
}
